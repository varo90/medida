<?php
error_reporting(E_ALL);
ini_set('display_errors', 'On');

include('header.php');
include('db_connections.php');
include('queries.php');

?>

<div style="margin-top:0;" id="imaginary_container"> 
    <div class="input-group stylish-input-group">
        <input id="search_text" type="text" class="form-control" placeholder="Nombre ó ID de cliente">
        <span class="input-group-addon">
            <button id="search_btn" type="submit">
                <span class="glyphicon glyphicon-search"></span>
            </button>  
        </span>
    </div>
</div>

<div id="information">
    <div class="container">
    <?php

    // if ( isset($_FILES["file"]["type"]) )
    // {
        $max_size = 500 * 1024; // 500 KB
        $client_id = $_POST['client_id'];
        $medida_id = $_POST['medida_id'];
        $coment = $_POST['coment'];

        $destination = "/var/www/sastreria/medida/imagenes_prendas/$client_id";
        if (!file_exists($destination)) {
            mkdir($destination, 0777, true);
        }
        $nom_fich = basename($_FILES['uploadedfile']['name']);
        $extension = explode('.', $nom_fich)[1];
        // $destination = 'uploaded/'.$nom_fich;

        if (move_uploaded_file($_FILES['uploadedfile']['tmp_name'], "$destination/$nom_fich")) {
            echo '<h1>Imagen subida</h1>';
            $db_my = new db('my','medidas');
            $query = $db_my->conn->prepare(queries::insert_image());
            $query->execute([$client_id,$medida_id,$nom_fich,$coment]);
            unset($db_my);
            header("location:index.php?status=success&client_id=$client_id");
        } else {
            header("location:index.php?status=fail&client_id=$client_id");
        }
    // }
    ?>
    </div>
</div>
