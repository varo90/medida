<?php
    include('db_connections.php');
    include('queries.php');

    $search_text = '%'.$_POST['search_text'].'%';

    $db_ms = new db();
    $clients = $db_ms->make_query(queries::get_clients(),[$search_text,$search_text]);
    unset($db_ms);
?>
<div class="container">
    <?php
        foreach($clients as $cont => $client) {
    ?>
            <button type="button" onclick="search_id('<?php echo $client->id; ?>','<?php echo $client->cliname; ?>')" class="btn btn-primary btn-round-sm btn-sm buttonclient"><?php echo "$client->id - $client->cliname" ?></button><br>
    <?php
        }
    ?>
</div>