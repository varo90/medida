<?php
    include('db_connections.php');
    include('queries.php');
?>
<div class="container">
    <form id="upload-image-form" action="insertar.php" method="post" enctype="multipart/form-data">
        <input name="uploadedfile" type="file" accept="image/*" capture="camera">
        <br>
        <textarea id="coment" class="form-control" name="coment" maxlength="200" rows="7" cols="20" placeholder="Comentario"></textarea>
        <input type="hidden" name="client_id" value="<?php echo $_POST['client_id']; ?>">
        <input type="hidden" name="medida_id" value="<?php echo $_POST['medida_id']; ?>">
        <br>
        <button type="submit" role="button" class="btn btn-primary btn-round-sm btn-sm">Enviar</button>
    </form>
</div>