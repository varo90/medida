<?php

class queries {
    static function get_clients() {
        $sql = "SELECT cli.CardCode as id, cli.CardName as cliname
                FROM SBO_EULALIA.dbo.OCRD cli WITH (NOLOCK)
                WHERE cli.CardName LIKE ? OR cli.CardCode LIKE ?
                ORDER BY cli.CardCode";
        return $sql;
    }

    static function get_amedida() {
        $sql = "SELECT med.Code AS code, med.U_GSP_STATUS AS status, med.U_GSP_DATE AS date, med.U_GSP_DATECOMMIT AS datecommit
                FROM SBO_EULALIA.dbo.[@GSP_SEADJUST] med
                    LEFT JOIN SBO_EULALIA.dbo.OITM oi ON med.U_GSP_ITEMCODE = oi.ItemCode
                WHERE med.U_GSP_CARDCODE = ? AND (oi.U_GSP_COLLECTION BETWEEN 100 AND 199) AND med.U_GSP_STATUS <> 'X'
                ORDER BY med.Code DESC";
        return $sql;
    }

    static function get_amedida_name() {
        $sql = "SELECT * FROM nombre_arreglo WHERE id=?";
        return $sql;
    }

    static function insert_image() {
        $sql = "INSERT INTO medida(client_code,medida_code,picturename,coment) VALUES (?, ?, ?, ?)";
        return $sql;
    }

    static function get_imagenes() {
        $sql = "SELECT * FROM medida WHERE client_code = ? AND medida_code = ?";
        return $sql;
    }

    static function change_arreglo_name() {
        $sql = "INSERT INTO nombre_arreglo(id,nombre) VALUES (:id,:nombre) ON DUPLICATE KEY UPDATE nombre=:nombre";
        return $sql;
    }
    
    function join_conditions($filters,$sql_ref,$sql_col) {
        $texts = array();
        foreach($filters as $filter) {
            if($filter == null) {continue;}
            $texts[] = "($sql_ref='$filter->reference' AND $sql_col='$filter->color')";
        }
        $conditions = implode(' OR ', $texts);
        return $conditions;
    }
}
