<?php
    include('header.php');
    $search = '';
    if(isset($_GET['status'])) {
        $message = $_GET['status'] == 'success' ? 'Imagen subida con éxito' : 'No se pudo subir la imagen';
        $search = $_GET['client_id'];
?>
            <div id="message" class="<?php echo $_GET['status']; ?>"><?php echo $message; ?></div>
            <br>
<?php
    }
?>
<div style="margin-top:30;" id="imaginary_container"> 
    <div class="input-group stylish-input-group">
        <input id="search_text" placeholder="Introduce un nombre o un c&oacute;digo de cliente" type="text" class="form-control" value="<?php echo $search; ?>">
        <span class="input-group-addon">
            <button id="search_btn" type="submit">
                <span class="glyphicon glyphicon-search"></span>
            </button>  
        </span>
    </div>
</div>
<div id="information"></div>
</body>
</html>
<?php
if(isset($_GET['status'])) {
?>
<script>
    $(document).ready(function() {    
        $( "#search_btn" ).trigger( "click" );
    });
</script>

<?php
}
?>