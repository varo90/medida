$( document ).ready(function() {
	
});

$(document).on('keyup', '#search_text', function (e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if(code == 13) { //Enter keycode
        $( "#search_btn" ).trigger( "click" );
    }
});

$(document).on('click', '#search_btn', function (e) {
    var search_text = $('#search_text').val();
    if(search_text != '') {
        data = {"search_text" : search_text};
        $.ajax({
            data:  data,
            url:   'clientes.php',
            type:  'post',
            // dataType:"json",
            beforeSend: function( xhr ) {
                $('#information').html('<div class="contenedor"><img src="images/busy.gif" /></div>');
            },
            success:  function (response) {
                $('#information .contenedor').remove();
                $('#information').html(response);
            }
        });	
    } else {
        $('#information').html('<div class="contenedor">Introduce un nombre ó ID de cliente</div>');
    }
});

$(document).on('click', '.upload_pix', function (e) {
    var datos = $(this).attr('id').split('__');
    var client_id = datos[0];
    var medida_id = datos[1];
    data = {"client_id" : client_id, "medida_id" : medida_id};
    $.ajax({
        data:  data,
        url:   'new_picture.php',
        type:  'post',
        // dataType:"json",
        beforeSend: function( xhr ) {
            $('#information').html('<img src="images/busy.gif" />');
        },
        success:  function (response) {
            $('#information img').remove();
            $('#information').html(response);
        }
    });	
});

function search_id(client_id,client_fullname) {
    data = {"client_id" : client_id, "client_fullname" : client_fullname};
    $.ajax({
        data:  data,
        url:   'medidas.php',
        type:  'post',
        // dataType:"json",
        beforeSend: function( xhr ) {
            $('#information').html('<div id="searching"><img src="images/busy.gif" /><br>Buscando medidas del cliente ' + client_id + '</div>');
        },
        success:  function (response) {
            $('#information #searching').remove();
            $('#information').html(response);
        }
    });	
}

function volver(client_id,medida_id) {
    $('#search_text').val(client_id);
    data = {"search_text" : client_id};
    $.ajax({
        data:  data,
        url:   'clientes.php',
        type:  'post',
        // dataType:"json",
        beforeSend: function( xhr ) {
            $('#information').html('<div class="contenedor"><img src="images/busy.gif" /></div>');
        },
        success:  function (response) {
            $('#information .contenedor').remove();
            $('#information').html(response);
        }
    });	
    // eventFire(document.getElementById('#search_btn'), 'click');
}

function save_arreglo_name(id) {
    var hacheteemeele = $('#changename__btn__'+id).html();
    var name = $('#nom_arreglo__'+id).val();
    data = {"id" : id, "name" : name};
    $.ajax({
        data:  data,
        url:   'change_name.php',
        type:  'post',
        // dataType:"json",
        beforeSend: function( xhr ) {
            $('#changename__btn__'+id).html('<img src="images/busy.gif" />');
        },
        success:  function (response) {
            $('#changename__btn__'+id+' img').remove();
            $('#changename__btn__'+id).html(hacheteemeele);
            $('#changename__input__'+id).val(response);
        }
    });	
}



/**************************** */
$(function() {    
    var isXS = false,
        $accordionXSCollapse = $('.accordion-xs-collapse');

    // Window resize event (debounced)
    var timer;
    $(window).resize(function () {
        if (timer) { clearTimeout(timer); }
        timer = setTimeout(function () {
            isXS = Modernizr.mq('only screen and (max-width: 767px)');
            
            // Add/remove collapse class as needed
            if (isXS) {
                $accordionXSCollapse.addClass('collapse');               
            } else {
                $accordionXSCollapse.removeClass('collapse');
            }
        }, 100);
    }).trigger('resize'); //trigger window resize on pageload    
    
    // Initialise the Bootstrap Collapse
    $accordionXSCollapse.each(function () {
        $(this).collapse({ toggle: false });
    });      
    
    // Accordion toggle click event (live)
    $(document).on('click', '.accordion-xs-toggle', function (e) {
        e.preventDefault();
        
        var $thisToggle = $(this),
            $targetRow = $thisToggle.parent('.tr'),
            $targetCollapse = $targetRow.find('.accordion-xs-collapse');            
    	
        if (isXS && $targetCollapse.length) { 
            var $siblingRow = $targetRow.siblings('.tr'),
                $siblingToggle = $siblingRow.find('.accordion-xs-toggle'),
                $siblingCollapse = $siblingRow.find('.accordion-xs-collapse');
            
            $targetCollapse.collapse('toggle'); //toggle this collapse
            $siblingCollapse.collapse('hide'); //close siblings
            
            $thisToggle.toggleClass('collapsed'); //class used for icon marker
            $siblingToggle.removeClass('collapsed'); //remove sibling marker class
        }
    });
});