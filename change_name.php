<?php
    error_reporting(E_ALL);
    ini_set('display_errors', 'On');
    include('db_connections.php');
    include('queries.php');
    
    $db_my = new db('my','medidas');
    
    $query = $db_my->conn->prepare(queries::change_arreglo_name());
    $query->bindParam(':id', $_POST['id'], PDO::PARAM_INT);
    $query->bindParam(':nombre', $_POST['name'], PDO::PARAM_STR, 50);
    $query->execute();

    unset($db_my);

    //echo '<font color="green">Hecho</font>';
    echo $_POST['name'];