<?php

date_default_timezone_set('Europe/Madrid');

class db {
    var $conn = null;
    var $typecon = 'ms';
    var $servername = 'srvsql';
    var $db = 'SBO_EULALIA';
    var $username = '******';
    var $password = '******';
    
    function __construct() {
        if(func_num_args() > 0) {
            $infoConn = func_get_args();
            if($infoConn[0] == 'ms_edit') {
                $this->username='******';
                $this->password='******';
            } else if($infoConn[0] == 'my') {
                $this->typecon = 'my';
                $this->servername = 'localhost';
                $this->db = $infoConn[1];
                $this->username = '******';
                $this->password = '******';
            }
        }
        $this->connect();
    }
    
    function __destruct() {
        $this->disconnect();
    }
    
    function connect() {
        try {
            if($this->typecon == 'ms') {
                $this->conn = new PDO("sqlsrv:Server=$this->servername;Database=$this->db", $this->username, $this->password);
                $this->conn->setAttribute(PDO::SQLSRV_ATTR_ENCODING, PDO::SQLSRV_ENCODING_UTF8);
            } else if ($this->typecon == 'my') {
                $this->conn = new PDO("mysql:host=$this->servername;dbname=$this->db", $this->username, $this->password);
            }
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);    // set the PDO error mode to exception
        }
        catch(PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
        }
    }
    
    function make_query() {
        $arg_list = func_get_args();
        $sql = $arg_list[0]; $params = $arg_list[1];
        $type = func_num_args() == 3 ? $arg_list[2] : PDO::FETCH_OBJ;
        $query = $this->conn->prepare($sql);
        $query->execute($params);
        return $query->fetchAll($type);
    }
    
    function disconnect() {
        $this->conn = null;
    }
}