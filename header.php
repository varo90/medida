<?php
include('session_init.php');
$fich = basename($_SERVER['SCRIPT_FILENAME']);
if ((empty($_SESSION['username_link']) || !isset($_SESSION['username_link'])) && $fich != 'login.php') {
    header("location:login.php");
}
?>
<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Medidas</title>
  <link href="dist/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="dist/css/medidas.css">
  <script src="dist/js/jquery.min.js"></script> 
  <script src="dist/js/bootstrap.min.js"></script> 
  <script src="dist/js/modernizr.min.js"></script>
  <script src="dist/js/medidas.js"></script>
</head>
<body>
<!-- Navigation -->
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
            <a class="navbar-brand" href="../link/index.php"><img src='images/logo-se.png' alt='SE Logo'></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav navbar-right">
            <?php
                include('session_init.php');
                if(empty($_SESSION['username_link']) || !isset($_SESSION['username_link'])) { 
            ?>
                    <li class="nav-item">
                      <a class="nav-link" href="login.php">Acceder</a>
                    </li>
            <?php
            }
            else { ?>
                    <li class="nav-item">
                      <a class="nav-link" href=""><?php echo $_SESSION['username_link']; ?></a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="logout.php">Salir</a>
                    </li>
            <?php
            }
            ?>
          </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>