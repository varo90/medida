<?php
    error_reporting(E_ALL);
    ini_set('display_errors', 'On');
    include('db_connections.php');
    include('queries.php');

    $db_ms = new db();
    $medidas = $db_ms->make_query(queries::get_amedida(),[$_POST['client_id']]);
    unset($db_ms);
?>  

  <br>

  <center>
    <h4><b><?php echo $_POST['client_id'] . ' - ' . $_POST['client_fullname']; ?></b></h4>
  </center>

  <br>

  <div class="divtable accordion-xs">
    <div class="tr headings">
      <div class="th firstname">Cliente</div>
      <div class="th lastname">Fotos</div>
      <div class="th username">Informaci&oacute;n</div>
    </div>
    <?php

    $db_my = new db('my','medidas');
    $query = $db_my->conn->prepare(queries::get_amedida_name());
    foreach($medidas as $cont => $medida) {
      $client_id = $_POST['client_id'];
      $query->execute([$medida->code]);
      $nombre = $query->fetch(PDO::FETCH_OBJ);
      if($nombre != false && $nombre->nombre != '' && $nombre->nombre != NULL) {
        $name_arreglo = $nombre->nombre;
        $nom_arr = $nombre->nombre;
      } else {
        $name_arreglo = $medida->code;
        $nom_arr = '';
      } 
    ?>
    <div class="tr">
      <?php $color = $medida->status != 'O' ? 'green' : 'red'; ?>
      <!-- <div id="medida_code" style="display: none;"><?php //echo $name_arreglo ?></div> -->
      <div style="background-color: <?php echo $color; ?>;" class="td firstname accordion-xs-toggle"><b><?php echo $name_arreglo . ' => Ini:' . date('d-m-Y', strtotime($medida->date)); ?></b></div>
      <div class="accordion-xs-collapse collapse" aria-expanded="false">
        <div id="inner__<?php echo $medida->code ?>" class="inner">
          <div class="td arregloname">
            <div id="changename__<?php echo $medida->code ?>" class="row">
              <div id="changename__input__<?php echo $medida->code ?>" class="col-xs-9">
                <input id="nom_arreglo__<?php echo $medida->code ?>" type="text" class="form-control" value="<?php echo $nom_arr ?>" placeholder="Nombre del arreglo">
              </div>
              <div id="changename__btn__<?php echo $medida->code ?>" class="col-xs-2">
                <button id="btn_arreglo__<?php echo $medida->code ?>" type="button" class="btn btn-success btn-round-sm btn-sm" onclick="save_arreglo_name(<?php echo $medida->code ?>)">Nombrar</button>
              </div>
            </div>
          </div>
          <div class="td lastname"><iframe style="width:100%;height:340px;" src="slider/standard.php?client_id=<?php echo $client_id; ?>&medida_id=<?php echo $medida->code; ?>"></iframe></div>
          <div class="td username"><button id="<?php echo $client_id . '__' . $medida->code; ?>" type="button" class="btn btn-success btn-round-sm btn-sm upload_pix">Subir foto</button></div><br>
        </div>
      </div>
    </div>
    <?php
    }
    unset($db_my);
    ?>
  </div>